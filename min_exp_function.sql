CREATE FUNCTION `FindMinExp` ()
RETURNS decimal(10,1)
BEGIN

RETURN (SELECT MIN(experience) FROM  employee );
END