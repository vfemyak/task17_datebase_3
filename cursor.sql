CREATE DEFINER=`root`@`localhost` PROCEDURE `CreateTableCursor`()
begin
   declare done int default false;
   declare NameEmp char(30);
   declare _Cursore cursor for
   select name from employee;
   
   declare continue handler for not found
   set done=true;
   
   open _Cursore;
   
   myLoop: loop 
          fetch _Cursore into NameEmp;
          if done = true then leave myLoop;
          end if;
          
          set @t_query = concat('create table IF NOT EXISTS', ' ' ,NameEmp , '(' , 'id int' , 'surname varchar(10)', 'identity_number char(10)',  ')' , ';' );
          
          prepare myQuery from @t_query;
          execute myQuery;
          deallocate prepare myQuery;
          
          end loop;
          close _Cursore;
    end