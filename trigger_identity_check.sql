delimiter //
create trigger forbid_update_post
before update on post for each row

begin
if(select count(*) from post where post=new.post) then
signal sqlstate '45000' set message_text = 'Update forbidden';
end if;
end //
delimiter ;