delimiter //
create trigger BeforeInsertPharmacy
before insert on pharmacy for each row

begin
if(select count(*) from street where street = new.street)=0 then
 SIGNAL SQLSTATE '45000' SET MYSQL_ERRNO = 30001, MESSAGE_TEXT = " Can't insert record. Foreign pharmacy  key doesn't exist";
 end if;
 end //
 delimiter ;